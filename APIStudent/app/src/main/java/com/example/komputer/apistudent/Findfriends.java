package com.example.komputer.apistudent;

public class Findfriends {
    public String profileimage, fullname, Faculty;


    public Findfriends()
    {

    }

    public String getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFaculty() {
        return Faculty;
    }

    public void setFaculty(String faculty) {
        Faculty = faculty;
    }

    public Findfriends(String profileimage, String fullname, String faculty) {
        this.profileimage = profileimage;
        this.fullname = fullname;
        Faculty = faculty;
    }
}
