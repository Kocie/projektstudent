package com.example.komputer.apistudent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import de.hdodenhof.circleimageview.CircleImageView;

public class SetupActivity extends AppCompatActivity {

    private EditText Username, FullName, FieldOfStudy;
    private Button SaveInfButton;
    private CircleImageView ProfileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        Username = (EditText) findViewById(R.id.setup_username);
        FullName = (EditText) findViewById(R.id.setup_fullname);
        FieldOfStudy = (EditText) findViewById(R.id.setup_fieldofstudy);
        SaveInfButton = (Button) findViewById(R.id.setup_inf_save_button);
        ProfileImage = (CircleImageView) findViewById(R.id.setup_profile_image);

        }





}
