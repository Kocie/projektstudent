package com.example.komputer.apistudent;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import de.hdodenhof.circleimageview.CircleImageView;

public class FindFriendsActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private ImageButton SearchButton;
    private EditText SearchInputText;

    private RecyclerView SearchResultList;

    private DatabaseReference allUsersDatabaseRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);


        allUsersDatabaseRef = FirebaseDatabase.getInstance().getReference().child("Users");

        mToolbar = (Toolbar) findViewById(R.id.find_friends_bar_layout);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Searching Friend");

        SearchResultList = (RecyclerView) findViewById(R.id.search_result_list);
        SearchResultList.setHasFixedSize(true);
        SearchResultList.setLayoutManager(new LinearLayoutManager(this));

        SearchButton = (ImageButton) findViewById(R.id.search_friends_button);
        SearchInputText = (EditText) findViewById(R.id.search_box_input);


        SearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 String searchBoxInput =  SearchInputText.getText().toString();

                SearchFriends(searchBoxInput);
            }
        });


    }

    private void SearchFriends(String searchBoxInput) {

        Toast.makeText(this, "Searching", Toast.LENGTH_SHORT).show();

        Query searchFriendsQuery  = allUsersDatabaseRef.orderByChild("fullname")
                .startAt(searchBoxInput).endAt(searchBoxInput + "\uf8ff");

        FirebaseRecyclerAdapter<Findfriends, FindFriendsViewHolder> firebaseRecyclerAdapter
                = new FirebaseRecyclerAdapter<Findfriends, FindFriendsViewHolder>
                (
                        Findfriends.class,
                        R.layout.all_users_display_layout,
                        FindFriendsViewHolder.class,
                        allUsersDatabaseRef
                )
        {
            @Override
            protected void populateViewHolder(FindFriendsViewHolder viewHolder, Findfriends model, int position) {

                viewHolder.setFullname(model.getFullname());
                viewHolder.setFaculty(model.getFaculty());
                viewHolder.setProfileimage(getApplicationContext(), model.profileimage);
            }
        };

        SearchResultList.setAdapter(firebaseRecyclerAdapter);

    }

    public static class FindFriendsViewHolder extends RecyclerView.ViewHolder
    {

        View mView;


        public FindFriendsViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }


        public void setProfileimage(Context ctx, String profileimage)
        {
            CircleImageView myImage = (CircleImageView) mView.findViewById(R.id.all_users_profile_image);
           // Picasso.with(ctx).load(profileimage).placeholder(R.drawable.profile_default).into();

        }

        public void setFullname(String fullname)
        {
            TextView myName = (TextView) mView.findViewById(R.id.all_users_profile_name);
            myName.setText(fullname);
        }

        public void setFaculty(String faculty)
        {
            TextView myfaculty = (TextView) mView.findViewById(R.id.all_users_status);
            myfaculty.setText(faculty);
        }



    }
}
